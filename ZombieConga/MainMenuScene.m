//
//  MainMenuScene.m
//  ZombieConga
//
//  Created by Brandon Levasseur on 10/20/13.
//  Copyright (c) 2013 Brandon Levasseur. All rights reserved.
//

#import "MainMenuScene.h"
#import "MyScene.h"

@implementation MainMenuScene

-(instancetype)initWithSize:(CGSize)size
{
    if (self = [super initWithSize:size]) {
        SKSpriteNode *bg = [[SKSpriteNode alloc] initWithImageNamed:@"MainMenu.png"];
        bg.position = CGPointMake(self.size.width / 2, self.size.height /2);
        
        [self addChild:bg];
    }
    
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    SKScene *myScene = [[MyScene alloc] initWithSize:self.size];
    [self.view presentScene:myScene transition:[SKTransition doorwayWithDuration:0.5]];
    
}


@end
